# @summary Manages the postfix service
#
class amazon_ses::service {
  service { 'postfix':
    ensure => running,
    enable => true,
  }
}
