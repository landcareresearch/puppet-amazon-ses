# Amazon SES Puppet Module Changelog

## 2020-10-01 Release 6.0.0

- Updated to comply with Puppet 6.
- Changed version to follow puppet 6 to show compliance.
- Removed the region_url2 in the sasl_passwd file as its no longer needed.

## 2020-03-13 Release 0.3.0

- Updated to comply with PDK Validate.
- Converted templates to epp.
- Added defined types to parameters.
- Removed anchor pattern.
- Updated vagrant puppetfile depedencies.

## 2019-11-01 Release 0.2.1

- Fixed source URL.

## 2019-11-01 Release 0.2.0

- Updated Changelog to comply with markdown linting.
- Changed API documentation to use puppet strings.
- Removed Ubuntu 12.04 support.

## 2018-05-16 Release 0.1.6

Added postfix parameter

### Changed

- Added the message_size_limit postfix parameter.

## 2017-02-09 Release 0.1.5

- Fixed minor lint error in config.pp

## 2015-11-06 Release 0.1.4

- Added support for overriding the default mynetworks configuration
- Removed Authors from all source files
- Created a contributors.txt
- Updated readme with new parameters

## 2015-03-04 Release 0.1.2

- Migrated from github to bitbucket
- Changed ownership of puppetforge account

## 2015-02-16 Release 0.1.1

- robinbowes has been very active with submitting improvements to this module including the following.
- Added additional parameters for postfix.
- Added the ability to use amazon region syntax for specifing regions.
- Ensured that the postfix service is started on vm reboot.
- Added support for redhat systems and tested on CentOS 7.
- Rebuilt password file when changes are made to postfix config file.

## 2015-01-16 Release 0.1.0

- Added TravisCI integration.
- Modified coding sytyle to comply with official puppet style.

## 2014-10-07 Release 0.0.2

https://github.com/koubas fixed the following issues

- Added a dependancy for postfix
- Corrected a syntax mistake in the configuration file
- Updated version

## 2014-08-28 Release 0.0.1

-Initial release.
