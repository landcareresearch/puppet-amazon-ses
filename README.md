# amazon_ses

[![Puppet Forge](http://img.shields.io/puppetforge/v/landcareresearch/amazon_ses.svg)](https://forge.puppetlabs.com/landcareresearch/amazon_ses)
[![Bitbucket Build Status](http://build.landcareresearch.co.nz/app/rest/builds/buildType%3A%28id%3ALinuxAdmin_PuppetAmazonSes_PuppetAmazonSes%29/statusIcon)](http://build.landcareresearch.co.nz/viewType.html?buildTypeId=LinuxAdmin_PuppetAmazonSes_PuppetAmazonSes&guest=1)

## Overview

Configures and sets up postfix to integrate with Amazon Simple Email Service (Amazon SES).

## Module Description

The module installs postfix and configures it to relay smtp to the Amazon SES smtp server.  
It uses the self-signed certs for TLS authentication with Amazon SES.  By default it connects to 
port 587 as this port does not have any restrictions.  Port 25 by default limits 1 email per minute,
if you choose port 25, make sure you apply for that restriction to be removed from your domain.

The intention of this module is to reduce the startup and configuration time of integration with
Amazon SES and to avoid simple postfix configuration errors.  

The current release is supported for Debian and Redhat based systems with Ubuntu as the tested platform.  

## Setup

### amazon_ses affects

* /etc/postfix/main.cf
* /etc/postfix/sasl_passwd
* /etc/postfix/sasl_passwd.db

### Setup Requirements

In order to use Amazon SES, you must login to your Amazon account and do the following.

#### Register for Amazon SES

Go to the following link and sign up.
([sign-up-for-aws](http://docs.aws.amazon.com/ses/latest/DeveloperGuide/sign-up-for-aws.html))

#### Create a SMTP user

Create a SMTP user which is separate from your existing IAM users.
The new user can be created via:

    SES -> smtp settings -> 'Create My SMTP Credentials' button.

Take note of the username and password which will used by this module.

#### Verify Email Addresses

In order to test this module (once installed on your amazon ec2 instance),
verify at least one email address that will be the recipient of your testing.
([verify-email-addresses](http://docs.aws.amazon.com/ses/latest/DeveloperGuide/verify-email-addresses.html))

#### Verify Domain

In order to test this module (once installed), verify the domain that emails
will be sent from.  Amazon's SMTP servers will reject emails from unverified domains.
So this is a very important step in order to start testing Amazon SES.  See the following guide:
([verify-domains](http://docs.aws.amazon.com/ses/latest/DeveloperGuide/verify-domains.html))

#### Amazon SES Production Access

Your Amazon SES instance is by default setup in a sandbox.  Once a domain and emails
have been verified, you can start sending emails (only to the verified addresses).  This obviously
is limiting and only useful in a testing environment.  Once you are ready to move to production,
you need to apply for production level access which has no restriction on recipient addresses.

Follow this guide to apply for production access:
([request-production-access](http://docs.aws.amazon.com/ses/latest/DeveloperGuide/request-production-access.html))

### Beginning with amazon_ses

To install postfix with Amazon SES configuration with the default parameters.

```puppet 
class { 'amazon_ses':
  domain => 'test.com',
  smtp_username => 'USERNAME',
  smtp_password => 'PASSWORD',
}
```

## API

See REFERENCE.md

### AWS Regions

The following table shows the relationship between the puppet option and the region.

| Puppet Option | Region Name | Region |
| ------------- | ----------- | ------ |
| `US EAST`     | N. Virginia | us-east-1 |
| `US WEST`     | Oregon      | us-west-2 |
| `EU`          | Ireland     | eu-west-1 |
| `us-east-1`   | N. Virginia | us-east-1 |
| `us-west-1`   | Oregon      | us-west-2 |
| `us-west-2`   | Oregon      | us-west-2 |
| `eu-west-1`   | Ireland     | eu-west-1 |
| `eu-central-1`| Ireland     | eu-west-1 |

## Limitations

Works with debian and redhat based OS's.

## Development

The module is open source and available on github.  Please fork!
